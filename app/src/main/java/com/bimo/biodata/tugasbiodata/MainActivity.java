package com.bimo.biodata.tugasbiodata;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private EditText cNama;
    private EditText cTtl;
    private EditText cAlamat;
    private RadioGroup rg_jk;
    private RadioButton rb_jk;
    private Button btnSimpan;
    private Button btnPesan;
    private Button btnExit;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rg_jk = (RadioGroup) findViewById(R.id.rg_jk);
        cNama = (EditText) findViewById(R.id.inputNama);
        cTtl = (EditText) findViewById(R.id.inputTtl);
        cAlamat = (EditText) findViewById(R.id.inputAlamat);
        btnPesan = (Button) findViewById(R.id.btnPesan);
        btnSimpan = (Button) findViewById(R.id.btnSimpan);
        btnExit = (Button) findViewById(R.id.btnExit);

        btnPesan.setOnClickListener(new ViewPesan());
        btnSimpan.setOnClickListener(new ViewSimpan());
        btnExit.setOnClickListener(new ViewExit());

    }

    class ViewPesan implements Button.OnClickListener {
        public void onClick (View v){
            String ttl;
            int usia,selectedJk;
            ttl = cTtl.getText().toString();
            usia = Integer.parseInt(ttl.substring(6, 10));
            int umur = 2018 - usia;
            selectedJk = rg_jk.getCheckedRadioButtonId();
            rb_jk = (RadioButton) findViewById(selectedJk);

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(
                    "Nama   : "+ cNama.getText()+"\n"+"\n"+
                    "Umur    : "+ umur +"\n"+"\n"+
                    "Jenis Kelamin  : " + rb_jk.getText()
            );


            builder.show();
        }
    }

    class ViewSimpan implements Button.OnClickListener {
        public void onClick (View v){

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(
                            "Nama   : "+ cNama.getText()+"\n"+"\n"+
                            "Alamat  : " + cAlamat.getText()
            );
            Toast.makeText(MainActivity.this, "Tersimpan", Toast.LENGTH_LONG).show();
            builder.show();

        }
    }

    class ViewExit implements Button.OnClickListener {
        public void onClick (View v){
            finish();
        }
    }
}
